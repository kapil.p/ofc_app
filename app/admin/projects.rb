ActiveAdmin.register Project do
  menu priority: 1
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
form do |f|
  inputs 'New Project' do
    input :email_subject
    input :client_email
    input :profile_name, as: :select, collection: DataStore.all.collect(&:profile_name)&.reject(&:blank?)
    input :profile_email, as: :select, collection: DataStore.all.collect(&:profile_email)&.reject(&:blank?)
    input :developer_name, as: :select, collection: DataStore.all.collect(&:developer_name)&.reject(&:blank?)
    input :bde_name, as: :select, collection: DataStore.all.collect(&:bde_member)&.reject(&:blank?)
    input :tech_stack, as: :select, collection: DataStore.all.collect(&:tech_stack)&.reject(&:blank?)
    input :deadline, :as => :datepicker
    input :status, :as => :select, :collection => DataStore.all.collect(&:status)&.reject(&:blank?)
  end
  actions
end

  permit_params :email_subject, :client_email, :profile_name, :profile_email, :deadline, :developer_name, :bde_name, :tech_stack, :status
  #
  # or
  #
  # permit_params do
  #   permitted = [:email_status, :client_email, :profile_name, :profile_email, :deadline, :developer_name, :bde_name, :tech_stack, :status]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

end
