ActiveAdmin.register DataStore do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :profile_name, :profile_email, :developer_name, :bde_member, :status, :tech_stack
  #
  # or
  #
  # permit_params do
  #   permitted = [:profile_name, :profile_email, :developer_name, :bde_member, :status, :tech_stack]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

end
