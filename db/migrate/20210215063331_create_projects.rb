class CreateProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :projects do |t|
      t.string   :email_subject
      t.string   :client_email
      t.string   :profile_name
      t.string   :profile_email
      t.datetime :deadline
      t.string   :developer_name
      t.string   :bde_name
      t.string   :tech_stack
      t.string   :status
      t.timestamps
    end
  end
end
