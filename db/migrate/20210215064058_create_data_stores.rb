class CreateDataStores < ActiveRecord::Migration[6.1]
  def change
    create_table :data_stores do |t|
      t.string :profile_name
      t.string :profile_email
      t.string :developer_name
      t.string :bde_member
      t.string :status
      t.string :tech_stack
      t.timestamps
    end
  end
end
